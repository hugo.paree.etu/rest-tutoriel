package fr.ulille.iut.tva.service;

/**
 * Tva
 */
public class CalculTva {
    public double calculerMontantTauxParDefaut(double somme) {
        return somme * (1 + TauxTva.NORMAL.taux / 100);
    }

    public double calculerMontant(TauxTva taux, double somme) {
        return somme * (1 + taux.taux / 100);
    }

    public double getTaux(TauxTva taux) {
        return taux.taux;
    }
}
