package fr.ulille.iut.tva.dto;

import jakarta.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SommeDto {
	private double montantTotal, montantTva, somme, tauxValue;
	private String tauxLabel;
	
	public SommeDto() {}
	
	public SommeDto(double montantTotal, double montantTva, double somme, double tauxValue, String tauxLabel) {
		super();
		this.montantTotal = montantTotal;
		this.montantTva = montantTva;
		this.somme = somme;
		this.tauxValue = tauxValue;
		this.tauxLabel = tauxLabel;
	}
	public double getMontantTotal() {
		return montantTotal;
	}
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getMontantTva() {
		return montantTva;
	}
	public void setMontantTva(double montantTva) {
		this.montantTva = montantTva;
	}
	public double getSomme() {
		return somme;
	}
	public void setSomme(double somme) {
		this.somme = somme;
	}
	public double getTauxValue() {
		return tauxValue;
	}
	public void setTauxValue(double tauxValue) {
		this.tauxValue = tauxValue;
	}
	public String getTauxLabel() {
		return tauxLabel;
	}
	public void setTauxLabel(String tauxLabel) {
		this.tauxLabel = tauxLabel;
	}
	
}
