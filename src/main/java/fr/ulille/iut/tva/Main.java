package fr.ulille.iut.tva;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.logging.LoggingFeature;
import org.glassfish.jersey.server.ResourceConfig;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Main class.
 *
 */
public class Main {
    // Base URI the Grizzly HTTP server will listen on
    public static final String BASE_URI = "http://localhost:8080/api/v1/";

    /**
     * Starts Grizzly HTTP server exposing JAX-RS resources defined in this application.
     * @return Grizzly HTTP server.
     */
    public static HttpServer startServer() {
        // create a resource config that scans for JAX-RS resources and providers
        // in fr.ulille.iut.tva package
        ResourceConfig rc = new ResourceConfig().packages("fr.ulille.iut.tva"); // <-- les paquetages dans lesquels Jersey cherche les ressources

        // Vous pouvez activer les log des requêtes et réponses sur le serveur
        // Pour cela avant de lancer le serveur, dans votre shell faites :
        // export LOG_MESSAGES=true
        String logging = System.getenv("LOG_MESSAGES");
        if ( logging != null && logging.equalsIgnoreCase("true") ) {
            rc.register(new LoggingFeature(Logger.getLogger(LoggingFeature.DEFAULT_LOGGER_NAME), Level.INFO,
                                           LoggingFeature.Verbosity.PAYLOAD_ANY, 10000));
        }

        // Si vous avez une erreur interne au serveur (HTTP 500), vous pouvez faire afficher l'erreur côté serveur
        // Attention, du coup toutes les erreurs seront renvoyées en 500, donc à désactiver quand vous avez compris le problème.
        // Pour activer, avant de lancer le serveur, dans votre shell faites :
        // export DEBUG_INTERNAL=true
        String debug = System.getenv("DEBUG_INTERNAL");
        if ( debug != null && debug.equalsIgnoreCase("true") ) {
            rc.register(DebugMapper.class);
        }

        // create and start a new instance of grizzly http server
        // exposing the Jersey application at BASE_URI
        return GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URI), rc);
    }

    /**
     * Main method.
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        final HttpServer server = startServer();
        System.out.println(String.format("Jersey app started with WADL available at "
                + "%sapplication.wadl\nHit enter to stop it...", BASE_URI));
        System.in.read();
        server.shutdownNow();
    }
}

